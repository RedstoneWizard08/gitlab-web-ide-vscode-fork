/* eslint-disable header/header */
import { ITelemetryAppender, NullAppender } from 'vs/platform/telemetry/common/telemetryUtils';

export class NoopAppender implements ITelemetryAppender {
	constructor(...args: any) { }

	log(eventName: string, data: any): void {
		return NullAppender.log(eventName, data);
	}

	flush(): Promise<any> {
		return NullAppender.flush();
	}
}
