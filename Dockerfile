FROM alpine

ARG TARGETPLATFORM

RUN mkdir /server && chmod 755 /server
WORKDIR /server

COPY .build/code-server/${TARGETPLATFORM} code-server
COPY scripts/gl/editor-injector/gl_start_server.sh start_server.sh
COPY scripts/gl/editor-injector/gl_copy_server.sh copy_server.sh

ENTRYPOINT ["./copy_server.sh"]
