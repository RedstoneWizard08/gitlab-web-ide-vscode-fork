#!/usr/bin/env bash

# This scripts builds the editor-injector.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)


docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# TODO: we don't build docker image for ARM in our CI yet. It works locally
# https://gitlab.com/gitlab-org/gitlab/-/issues/392693
docker build -t "${IMAGE_NAME}:$(cat FULL_VERSION)" .

# only push the image if we are on tag
if [ $CI_COMMIT_TAG ]; then
  docker push "${IMAGE_NAME}:$(cat FULL_VERSION)"
fi
